FROM ubuntu:22.04
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y tzdata=2023c-0ubuntu0.22.04.2 \
    && apt-get install --no-install-recommends -y curl=7.81.0-1ubuntu1.14 gnupg=2.2.27-3ubuntu2.1 \
    && apt-get install --no-install-recommends -y software-properties-common=0.99.22.7 \
    && apt-add-repository -y ppa:ansible/ansible \
    && apt-get update \
    && apt-get install --no-install-recommends -y ansible=8.5.0-1ppa~jammy python3-pip=22.0.2+dfsg-1ubuntu0.3 ssh=1:8.9p1-3ubuntu0.4 rsync=3.2.7-0ubuntu0.22.04.2 \
    && pip install --no-cache-dir ansible-lint==6.21.1 pylint==3.0.2 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
